// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "TankBarrel.generated.h"

/**
 * 
 */

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	//Functions
	void Elevate(float RelativeSpeed); // -1 is -MaxDegressPerSecond and 1 is MaxDegressPerSecond. Values bounded between -1 and 1
	
private:
	//Variables
	UPROPERTY(EditDefaultsOnly, Category = BarrelMobility)
		float MaxDegreesPerSecond = 10;
	UPROPERTY(EditDefaultsOnly, Category = BarrelMobility)
		float MaxElevation = 30;
	UPROPERTY(EditDefaultsOnly, Category = BarrelMobility)
		float MinElevation = 0;
};
