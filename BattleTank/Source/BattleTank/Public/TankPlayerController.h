// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "TankPlayerController.generated.h"

class ATankAimingComponent;

UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:	
	//Functions
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds);

protected:
	//Functions
	UFUNCTION(BlueprintImplementableEvent, Category = Setup)
		void FoundAimingComponent(UTankAimingComponent* AimingComponentReference);

private:
	//Functions
	void AimAtCrosshair();
	bool GetSightRayHitLocation(FVector& OUT Hitlocation) const;								//Returns true if something is hit and the location of what was hit as an out parameter.
	bool GetLookDirectionHitLocation(FVector LookDirection, FVector& OUT HitLocation) const;	//Returns true if something is hit and the location of what was hit as an out parameter. Requires a given look direction.
	virtual void SetPawn(APawn* InPawn) override;

	UFUNCTION()
		void OnPossessedTankDeath();

	//Variables
	UPROPERTY(EditDefaultsOnly)
		float CrosshairXLocation = .5;
	UPROPERTY(EditDefaultsOnly)
		float CrosshairYLocation = .333333333333333;
	UPROPERTY(EditDefaultsOnly)
		float LineTraceRange = 1000000.0;
};
