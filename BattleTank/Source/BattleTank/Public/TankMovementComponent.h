// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/NavMovementComponent.h"
#include "TankMovementComponent.generated.h"

class UTankTread;

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()

public:
	//Functions
	UFUNCTION(BlueprintCallable, Category = Setup)
		void Initialize(UTankTread* LeftTreadToSet, UTankTread* RightTreadToSet);
	UFUNCTION(BlueprintCallable, Category = Controls)
		void IntendedMoveX(float Throw);
	UFUNCTION(BlueprintCallable, Category = Controls)
		void IntendedMoveY(float Throw);


private:
	//Functions
	void RequestDirectMove(const FVector & MoveVelocity, bool bForceMaxSpeed) override;	//Returns velocity to move to location. Called by MoveToActor() in TankAIController every tick

	//Variables
	UTankTread * LeftTread = nullptr;
	UTankTread * RightTread = nullptr;
	
};
