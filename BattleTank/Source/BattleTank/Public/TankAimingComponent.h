// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Core/Public/Math/Vector.h"
#include "TankAimingComponent.generated.h"

UENUM()
enum class EFiringStatus : uint8
{
	Aiming,
	Locked,
	Reloading,
	OutOfAmmo
};

class UTankTurret;
class UTankBarrel;
class AProjectile;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	//Functions
	void AimAt(FVector HitLocation);
	UFUNCTION(BlueprintCallable, Category = Setup)
		void Initialize(UTankTurret* TurretToSet, UTankBarrel* BarrelToSet);
	UFUNCTION(BlueprintCallable, Category = Firing)
		void Fire();
	EFiringStatus GetFiringStatus() const;
	UFUNCTION(BlueprintCallable, Category = Firing)
		int32 GetAmmo() const;

	//Variables
	UTankBarrel *Barrel = nullptr;
	UTankTurret *Turret = nullptr;

protected:
	//Variables
	UPROPERTY(BlueprintReadOnly, Category = Status)
		EFiringStatus Status = EFiringStatus::Reloading;
	//UPROPERTY(BlueprintReadOnly, Category = Firing)		//Why does this not work?
		//int32 Ammo = 10;

private:
	//Functions
	UTankAimingComponent();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	bool IsBarrelMoving();
	void MoveBarrelTowards(FVector AimDirection);

	//Variables
	UPROPERTY(EditAnywhere, Category = Firing)
		float LaunchSpeed = 4000;
	UPROPERTY(EditAnywhere, Category = Firing)
		float ReloadTimeInSeconds = 3;
	UPROPERTY(EditAnywhere, Category = Firing)
		int32 Ammo = 10;
	UPROPERTY(EditDefaultsOnly, Category = Setup)
		TSubclassOf<AProjectile> ProjectileBluePrint;
	FVector AimDirection;
	double LastFireTime = 0;
};
