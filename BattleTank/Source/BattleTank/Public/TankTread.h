// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "TankTread.generated.h"

/**
* TankTread is used to set maximum driving force and apply forces to the tank.
**/

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTread : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	//Functions
	UFUNCTION(BlueprintCallable, Category = Controls)
		void SetThrottle(float Throttle);


private:
	//Functions
	UTankTread();
	virtual void BeginPlay() override;
	void ApplySidewayForce();
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
	void DriveTread();

	//Variables
	UPROPERTY(EditDefaultsOnly, Category = TreadMobility)
		float MaxTreadDrivingForce = 20000000.0;	//Assume 40 metric ton tank
	float CurrentThrottle = 0;
	
};
