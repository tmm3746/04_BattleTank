// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "TankTurret.generated.h"



UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTurret : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	//Functions
	void Rotate(float RelativeSpeed); // -1 is -MaxDegressPerSecond and 1 is MaxDegressPerSecond. Values bounded between -1 and 1

private:
	//Variables
	UPROPERTY(EditDefaultsOnly, Category = TurretMobility)
		float MaxDegreesPerSecond = 25;	
};
