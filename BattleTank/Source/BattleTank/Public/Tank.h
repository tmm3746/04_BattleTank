// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/GameFramework/Pawn.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectBaseUtility.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Tank.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTankDelegate);

UCLASS()
class BATTLETANK_API ATank : public APawn
{
	GENERATED_BODY()

public:
	//Functions
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;	// Called by the engine when actor damage is dealt
	UFUNCTION(BlueprintPure, Category = Health)
	float GetHealthPercent() const;	//Returns current health as a percentage of starting health between 0 and 1

	FTankDelegate OnDeath;

protected:
	

private:
	//Functions
	ATank();
	virtual void BeginPlay() override;

	//Variables
	UPROPERTY(EditDefaultsOnly, Category = Setup)
		int32 MaximumHealth = 100;
	UPROPERTY(VisibleAnywhere, Category = Health)
		int32 CurrentHealth;	//Initialized in Begin Play
};
