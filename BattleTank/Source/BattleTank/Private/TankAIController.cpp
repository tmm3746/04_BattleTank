// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "TankAimingComponent.h"
#include "Tank.h"

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
}

void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		ATank* PossessedTank = Cast<ATank>(InPawn);
		if (PossessedTank)
		{
			// Subscribe our local method to the tank's death event
			PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnPossessedTankDeath);
		}
	}
}

void ATankAIController::OnPossessedTankDeath()
{
	if (ensure(GetPawn()))
	{
		GetPawn()->DetachFromControllerPendingDestroy();
	}
}

void ATankAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	APawn *PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();

	if (PlayerTank && GetPawn())
	{
		//MoveTowardsPlayerTank
		MoveToActor(PlayerTank, AcceptanceRadius);
		//Aim at player tank
		GetPawn()->FindComponentByClass<UTankAimingComponent>()->AimAt(PlayerTank->GetActorLocation());
		//Fire at player if locked
		if (GetPawn()->FindComponentByClass<UTankAimingComponent>()->GetFiringStatus() == EFiringStatus::Locked)
		{
			GetPawn()->FindComponentByClass<UTankAimingComponent>()->Fire();
		}
	}
}