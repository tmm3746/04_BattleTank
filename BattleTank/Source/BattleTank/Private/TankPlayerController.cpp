// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"
#include "Tank.h"
#include "TankAimingComponent.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
	UTankAimingComponent *AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (ensure(AimingComponent))
	{
		FoundAimingComponent(AimingComponent);
	}
}

void ATankPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	AimAtCrosshair();
}

void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (PossessedTank)
		{
			// Subscribe our local method to the tank's death event
			PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnPossessedTankDeath);
		}
	}
}

void ATankPlayerController::OnPossessedTankDeath()
{
	StartSpectatingOnly();
}

void ATankPlayerController::AimAtCrosshair()
{
	if (GetPawn() && GetPawn()->FindComponentByClass<UTankAimingComponent>())
	{
		FVector OUT HitLocation;
		if (GetSightRayHitLocation(OUT HitLocation))
		{
			GetPawn()->FindComponentByClass<UTankAimingComponent>()->AimAt(HitLocation);
		}
	}
}

//Returns true if something is hit and the location of what was hit as an out parameter.
bool ATankPlayerController::GetSightRayHitLocation(FVector& OUT HitLocation) const
{
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(OUT ViewportSizeX, OUT ViewportSizeY);
	FVector2D CrosshairLocation(ViewportSizeX * CrosshairXLocation, ViewportSizeY * CrosshairYLocation);
	FVector CameraWorldLocation, LookDirection; // CameraWorldLocation is discarded.

	//Convert 2D screen position to World Space 3D position and direction. Returns false if unable to determine value.
	if (DeprojectScreenPositionToWorld(CrosshairLocation.X, CrosshairLocation.Y, OUT CameraWorldLocation, OUT LookDirection))
	{
		if (GetLookDirectionHitLocation(LookDirection, OUT HitLocation))
		{
			return true;
		}
	}

	return false;
}

//Returns true if something is hit and the location of what was hit as an out parameter. Requires a given look direction.
bool ATankPlayerController::GetLookDirectionHitLocation(FVector LookDirection, FVector& OUT HitLocation) const
{
	FHitResult HitResult;
	FVector Start(PlayerCameraManager->GetCameraLocation()), End(Start + LookDirection * LineTraceRange);

	if (GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_Camera))
	{
		OUT HitLocation = HitResult.Location;
		return true;
	}
	return false;
}