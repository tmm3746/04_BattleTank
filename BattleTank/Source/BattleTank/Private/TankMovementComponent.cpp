// Fill out your copyright notice in the Description page of Project Settings.

#include "TankMovementComponent.h"
#include "TankTread.h"

void UTankMovementComponent::Initialize(UTankTread* LeftTreadToSet, UTankTread* RightTreadToSet)
{
	LeftTread = LeftTreadToSet;
	RightTread = RightTreadToSet;
}
void UTankMovementComponent::IntendedMoveX(float Throw)
{
	if (LeftTread && RightTread)
	{
		LeftTread->SetThrottle(Throw);
		RightTread->SetThrottle(Throw);
	}
}

void UTankMovementComponent:: IntendedMoveY(float Throw)
{
	if (LeftTread && RightTread)
	{
		LeftTread->SetThrottle(Throw);
		RightTread->SetThrottle(-Throw);
	}
}

void UTankMovementComponent::RequestDirectMove(const FVector & MoveVelocity, bool bForceMaxSpeed)
{
	//Replacing functionality, no need to call super

	FVector AIMoveDirection = MoveVelocity.GetSafeNormal();
	FVector TankForward = GetOwner()->GetActorForwardVector();
	float AIVerticalThrow = FVector::DotProduct(AIMoveDirection, TankForward);
	float AIHorizontalThrow = FVector::CrossProduct(TankForward, AIMoveDirection).Z;
	IntendedMoveX(AIVerticalThrow);
	IntendedMoveY(AIHorizontalThrow);
}
