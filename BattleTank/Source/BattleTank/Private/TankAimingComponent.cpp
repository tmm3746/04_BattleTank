// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Projectile.h"

UTankAimingComponent::UTankAimingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
	LastFireTime = FPlatformTime::Seconds();
}

void UTankAimingComponent::Initialize(UTankTurret * TurretToSet, UTankBarrel * BarrelToSet)
{
	Turret = TurretToSet;
	Barrel = BarrelToSet;
}

void UTankAimingComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (Ammo <= 0)
	{
		Status = EFiringStatus::OutOfAmmo;
	}
	else if ((FPlatformTime::Seconds() - LastFireTime) < ReloadTimeInSeconds)
	{
		Status = EFiringStatus::Reloading;
	}
	else if (IsBarrelMoving())
	{
		Status = EFiringStatus::Aiming;
	}
	else
	{
		Status = EFiringStatus::Locked;
	}
}

void UTankAimingComponent::AimAt(FVector HitLocation)
{
	if (ensure(Barrel))
	{
		auto MyTankName = GetOwner()->GetName();
		auto MyBarrelLocation = Barrel->GetComponentLocation().ToString();
		FVector OUT LaunchVelocity;
		if (UGameplayStatics::SuggestProjectileVelocity(this, OUT LaunchVelocity, Barrel->GetSocketLocation(FName("ProjectileSpawn")), HitLocation, LaunchSpeed, false, 0, 0, ESuggestProjVelocityTraceOption::DoNotTrace))
		{
			AimDirection = LaunchVelocity.GetSafeNormal();
			MoveBarrelTowards(AimDirection);
		}
	}
}

bool UTankAimingComponent::IsBarrelMoving()
{
	if (ensure(Barrel))
	{
		FVector BarrelForward = Barrel->GetForwardVector();
		return !BarrelForward.Equals(AimDirection, .001);
	}
	return true;
}

void UTankAimingComponent::MoveBarrelTowards(FVector AimDirection)
{
	if (ensure(Barrel) && ensure(Turret))
	{
		FRotator BarrelRotator = Barrel->GetForwardVector().Rotation();
		FRotator AimAsRotator = AimDirection.Rotation();
		FRotator DeltaRotator = AimAsRotator - BarrelRotator;
		Barrel->Elevate(DeltaRotator.Pitch);
		//Always rotate shortest route
		if (FMath::Abs(DeltaRotator.Yaw) < 180.0)
		{
			Turret->Rotate(DeltaRotator.Yaw);
		}
		else
		{
			Turret->Rotate(-DeltaRotator.Yaw);
		}
	}
}

//Fires a projectile
void UTankAimingComponent::Fire()
{
	if (Status != EFiringStatus::Reloading && Status != EFiringStatus::OutOfAmmo)
	{
		if (ensure(Barrel) && ensure(ProjectileBluePrint))
		{
			auto Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileBluePrint, Barrel->GetSocketLocation(FName("ProjectileSpawn")), Barrel->GetSocketRotation(FName("ProjectileSpawn")));
			Projectile->LaunchProjectile(LaunchSpeed);
			LastFireTime = FPlatformTime::Seconds();
			Status = EFiringStatus::Reloading;
			Ammo--;
		}
	}
	
}

EFiringStatus UTankAimingComponent::GetFiringStatus() const
{
	return Status;
}

int32 UTankAimingComponent::GetAmmo() const
{
	return Ammo;
}